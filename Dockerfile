FROM debian:10 as build
WORKDIR /tmp/nginx-build

RUN apt update && \
    apt install -y \
    wget \
    gcc \
    make \
    build-essential && \

    wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20240314.tar.gz && \
    wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.3.tar.gz && \
    wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.28.tar.gz && \
    wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.13.tar.gz && \
    wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.26.tar.gz && \
    wget http://nginx.org/download/nginx-1.25.1.tar.gz && \
    wget http://zlib.net/zlib-1.3.1.tar.gz && \
    wget https://github.com/PCRE2Project/pcre2/releases/download/pcre2-10.44/pcre2-10.44.tar.gz && \

    # Install luajit2
    tar xzvf v2.1-20240314.tar.gz && \
    cd luajit2-2.1-20240314 && \
    make -j2 && \
    make install && \
    cd .. && \
    export LUAJIT_LIB=/usr/local/lib && \
    export LUAJIT_INC=/usr/local/include/luajit-2.1 && \

    # Install ngx_devel_kit
    tar xzvf v0.3.3.tar.gz && \

    # Install lua-nginx-module
    tar xzvf v0.10.26.tar.gz && \

    # Install zlib
    tar xzvf zlib-1.3.1.tar.gz && \

    # Install pcre2
    tar xzvf pcre2-10.44.tar.gz && \

    # Install nginx
    tar xzvf nginx-1.25.1.tar.gz && \
    cd nginx-1.25.1 && \
    ./configure \
    --with-ld-opt="-Wl,-rpath,/usr/local/lib" \
    --add-module=/tmp/nginx-build/ngx_devel_kit-0.3.3 \
    --add-module=/tmp/nginx-build/lua-nginx-module-0.10.26 \
    --with-zlib=/tmp/nginx-build/zlib-1.3.1 \
    --with-pcre=/tmp/nginx-build/pcre2-10.44 && \

    make -j2 && \
    make install && \
    cd .. && \

    # Install lua-resty-core
    tar xzvf v0.1.28.tar.gz && \
    cd lua-resty-core-0.1.28 && \
    make install LUA_LIB_DIR=/usr/local/share/lua/5.1 && \
    cd .. && \

    # Install lua-resty-lrucache
    tar xzvf v0.13.tar.gz && \
    cd lua-resty-lrucache-0.13 && \
    make install LUA_LIB_DIR=/usr/local/share/lua/5.1

FROM debian:10
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/ ../../
RUN chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]